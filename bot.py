from flask import Flask, request, Response, make_response
import json
from slacker import Slacker
import threading
import logging
logging.basicConfig(level=logging.DEBUG)


# Anonymous
with open('token.txt') as f:
    token =  f.readline()
slack = Slacker(token)
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'


def get_answer(query, channel):
    app.logger.info(f"query\n{query}")
    if '안녕' in query:
        slack.chat.post_message(channel, "안녕", as_user=True)
        return query
    elif 'APOD' in query:
        import requests
        from bs4 import BeautifulSoup
        from urllib.request import urlopen
        root = 'https://apod.nasa.gov/'
        webpage = requests.get('https://apod.nasa.gov/')
        soup = BeautifulSoup(webpage.content, "html.parser")
        img = soup.find_all('img')
        print(img[0]['src'])
        if len(img) < 1:
            slack.chat.post_message(channel, "APOD Image not found", as_user=True)
            return query
        else:
            image = root + img[0]['src']
            with urlopen(image) as f:
                import os.path
                if os.path.isfile('apod.jpg'):
                    os.remove('apod.jpg')
                with open('apod.jpg', 'wb') as h:  # w - write b - binary
                    img = f.read()
                    h.write(img)
            slack.files.upload('apod.jpg', channels=channel)
            return query
    else:
        return query


def event_handler(event_type, slack_event):
    channel = slack_event["event"]["channel"]
    if event_type == 'message':
    # if string_slack_event.find("{'type': 'message'") != -1:
        app.logger.info(f"slack event \n{slack_event}")
        try:
            user_query = slack_event['event']['text']
            if user_query[0] != '!':
                return make_response("doesn`t start with '!'", 200, )
            get_answer(user_query[1:], channel)
            return make_response("ok", 200, )
        except IndexError:
            pass
    message = "[%s] cannot find event handler" % event_type
    return make_response(message, 200, {"X-Slack-No-Retry": 1})


@app.route('/', methods=['POST'])
def hello_there():
    slack_event = json.loads(request.data)
    if "challenge" in slack_event:
        return make_response(slack_event["challenge"], 200, {"content_type": "application/json"})
    if "event" in slack_event:
        event_type = slack_event["event"]["type"]
        return event_handler(event_type, slack_event)
    return make_response("There are no slack request events", 404, {"X-Slack-No-Retry": 1})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

